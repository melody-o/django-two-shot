from django.urls import path
from accounts.views import user_login, user_logout, signup


# Register the function in your accounts
# urls.py with the path "login/" and the name "login".
urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", signup, name="signup")
]
