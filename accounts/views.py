from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from .forms import LoginForm
from django.contrib.auth.models import User
from accounts.forms import SignUpForm
# from django.contrib.auth.decorators import login_required

# Create your views here.


def user_login(request):
    # display a form when it is a GET method
    if request.method == "POST":
        # try to log the person in when it is a POST method
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(
                request,
                username=username,
                password=password,
            )
# If the person successfully logs in, redirect the browser to the URL
# registration named "home"
            if user:
                login(request, user)
                return redirect("home")
    else:
        form = LoginForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


def user_logout(request):
    logout(request)
    return redirect("login")


def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password")
            password_confirmation = form.cleaned_data.get(
                "password_confirmation"
            )

            if password == password_confirmation:
                user = User.objects.create_user(
                    username=username, password=password
                )

                login(request, user)
                return redirect("home")
            else:
                form.add_error("password", "Passwords do not match")
    else:
        form = SignUpForm()
    context = {"form": form}
    return render(request, "accounts/signup.html", context)


# @login_required
# def create_account(request):
#     if request.method == "POST":
#         form = ReceiptForm(request.POST)
#         if form.is_valid():
#             saved = form.save(False)
#             # makes sure the form isn't saved
#             # until we make sure we have a purchaser
#             saved.owner = request.user
#             # setting the purchaser as the user
#             saved.save()
#             # saved
#             return redirect("home")
#     else:
#         form = ReceiptForm()

#         context = {
#             "form": form,
#         }
#         return render(request, "receipts/create.html", context)
