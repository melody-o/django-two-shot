from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt

# Register your models here.
admin.site.register(ExpenseCategory)


class ExpenseCategory(admin.ModelAdmin):
    list_display = (
        "name",
        "owner"
    )


admin.site.register(Account)


class Account(admin.ModelAdmin):
    list_display = (
        "name",
        "number",
        "owner"
    )


admin.site.register(Receipt)


class Receipt(admin.ModelAdmin):
    list_display = (
        "vendor",
        "total",
        "tax",
        "date",
        "purchaser",
        "category",
        "account"
    )
