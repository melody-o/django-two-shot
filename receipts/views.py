from django.shortcuts import render, redirect
from receipts.models import Receipt, Account, ExpenseCategory
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm
# Create your views here.


@login_required
def receipt(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    # request.user means whos logged in
    context = {
        "receipt_list": receipt_list
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            saved = form.save(False)
            # makes sure the form isn't saved
            # until we make sure we have a purchaser
            saved.purchaser = request.user
            # setting the purchaser as the user
            saved.save()
            # saved
            return redirect("home")
    else:
        form = ReceiptForm()

        context = {
            "form": form,
        }
        return render(request, "receipts/create.html", context)


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    # request.user means whos logged in
    context = {
        "accounts": accounts
    }
    return render(request, "receipts/account_list.html", context)


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    receipts = Receipt.objects.filter(purchaser=request.user)
    # request.user means whos logged in
    context = {
        "categories": categories,
        "receipts": receipts
    }
    return render(request, "receipts/category_list.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            saved = form.save(False)
            # makes sure the form isn't saved
            # until we make sure we have a purchaser
            saved.owner = request.user
            # setting the purchaser as the user
            saved.save()
            # saved
            return redirect("category_list")
            # when using redirect, grab the "name=" from urls.py
    else:
        form = ExpenseCategoryForm()

        context = {
            "form": form,
        }
        return render(request, "receipts/create_category.html", context)
        # render always needs request, html, context


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            saved = form.save(False)
            # makes sure the form isn't saved
            # until we make sure we have a purchaser
            saved.owner = request.user
            # setting the purchaser as the user
            saved.save()
            # saved
            return redirect("account_list")
            # when using redirect, grab the "name=" from urls.py
    else:
        form = AccountForm()

        context = {
            "form": form,
        }
        return render(request, "receipts/create_account.html", context)
